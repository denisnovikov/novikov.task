<?php

namespace Novikov\Task\Entity;

use Bitrix\Main\ObjectNotFoundException;
use Bitrix\Main\Type\DateTime;
use Throwable;

class User
{
    public string $name;
    public string $lastName;
    public DateTime $lastLogin;
    public array $errors = [];

    public static function fromArray(?array $data): ?self
    {
        $entity = new self();

        try {
            if (!$data) {
                throw new ObjectNotFoundException('Передан пустой массив');
            }

            $entity->name = $data['NAME'];
            $entity->lastName = $data['LAST_NAME'];
            $entity->lastLogin = new DateTime($data['LAST_LOGIN']);
        } catch (Throwable $exception) {
            $entity->errors[] = $exception->getMessage();
        }

        return $entity;
    }
}
