<?php

namespace Novikov\Task\Entity;

use Bitrix\Main\Type\DateTime;

class ObjectUnit
{
    public int $objectId;
    public DateTime $objectDateCreate;
    public string $objectName;
    public string $objectAddress;
    public string $objectComment;
    public DateTime $dispatcherDateCreate;
    public bool $dispatcherActive;
    public DateTime $dispatcherActiveTo;
    public string $dispatcherComment;
    public string $userName;
    public int $userRole;
    public string $userLastName;
    public ?DateTime $userLastLogin;

    public static function fromArray(array $data): self
    {
        $entity = new self();

        $entity->objectId = $data['OBJECT_ID'];
        $entity->objectDateCreate = $data['OBJECT_DATE_CREATE'];
        $entity->objectName = $data['OBJECT_NAME'];
        $entity->objectAddress = $data['OBJECT_ADDRESS'];
        $entity->objectComment = $data['OBJECT_COMMENT'];

        $entity->dispatcherDateCreate = $data['DISPATCHER_DATE_CREATE'];
        $entity->dispatcherActive = $data['DISPATCHER_ACTIVE'];
        $entity->dispatcherActiveTo = $data['DISPATCHER_ACTIVE_TO'];
        $entity->dispatcherComment = $data['DISPATCHER_COMMENT'];

        $entity->userName = $data['USER_NAME'];
        $entity->userRole = $data['USER_ROLE'];
        $entity->userLastName = $data['USER_LAST_NAME'];
        $entity->userLastLogin = $data['USER_LAST_LOGIN'] ?: null;

        return $entity;
    }
}
