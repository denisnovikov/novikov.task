<?php

namespace Novikov\Task\Entity;

use Bitrix\Main\Type\DateTime;

class Dispatcher
{
    public int $id;
    public DateTime $dateCreate;
    public bool $isActive;
    public DateTime $activeTo;
    public int $userId;
    public int $userRoleId;
    public string $comment;

    public static function fromArray(array $data): self
    {
        $entity = new self();

        $entity->id = $data['ID'];
        $entity->dateCreate = $data['DATE_CREATE'];
        $entity->isActive = $data['ACTIVE'] === 'Y';
        $entity->activeTo = $data['ACTIVE_TO'];
        $entity->userId = $data['USER_ID'];
        $entity->userRoleId = $data['USER_ROLE'];
        $entity->comment = (string)$data['COMMENT'];

        return $entity;
    }
}
