<?php

namespace Novikov\Task\Repository;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\ORM\Objectify\EntityObject;
use Bitrix\Main\SystemException;
use Novikov\Task\Exception\RepositoryFindItemException;

interface RepositoryInterface
{
    /**
     * @param int $id
     *
     * @return EntityObject|null
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws RepositoryFindItemException
     * @throws SystemException
     */
    public function find(int $id): ?EntityObject;

    /**
     * @param array $criteria
     *
     * @return EntityObject|null
     * @throws RepositoryFindItemException
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function findOneBy(array $criteria): ?EntityObject;

    /**
     * @throws ObjectPropertyException
     * @throws SystemException
     * @throws ArgumentException
     */
    public function findAll(): array;

    /**
     * @param array $criteria
     *
     * @throws ObjectPropertyException
     * @throws SystemException
     * @throws ArgumentException
     */
    public function findBy(array $criteria): array;

    /**
     * @param int $id
     * @param array $arFields
     */
    public function updateFields(int $id, array $arFields): void;
}
