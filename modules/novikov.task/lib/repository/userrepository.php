<?php

namespace Novikov\Task\Repository;

use CUser;
use Novikov\Task\entity\User;
use Novikov\Task\System\Repository;

class UserRepository extends Repository
{
    /**
     * @param int $profileId
     * @return User
     */
    public function getProfileData(int $profileId): ?User
    {
        $obResult = CUser::GetByID($profileId);
        if ($arResult = $obResult->Fetch()) {
            return User::fromArray($arResult);
        }

        return null;
    }
}
