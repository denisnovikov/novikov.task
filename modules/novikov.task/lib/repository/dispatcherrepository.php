<?php

namespace Novikov\Task\Repository;

use Bitrix\Main\ORM\Objectify\EntityObject;
use Novikov\Task\Exception\RepositoryFindItemException;
use Novikov\Task\Orm\EntityDispatcherTable;
use Novikov\Task\System\Repository;

class DispatcherRepository extends Repository implements RepositoryInterface
{
    public function find(int $id): ?EntityObject
    {
        $obResult = EntityDispatcherTable::getByPrimary([$id])->fetchObject();
        if (!$obResult) {
            throw new RepositoryFindItemException('Элемент не найден');
        }

        return $obResult;
    }

    public function findOneBy(array $criteria): ?EntityObject
    {
        $obResult = EntityDispatcherTable::getList(['filter' => $criteria])->fetchObject();
        if (!$obResult) {
            throw new RepositoryFindItemException('Элемент не найден');
        }

        return $obResult;
    }

    public function findAll(): array
    {
        $arResult = EntityDispatcherTable::getList()->fetchAll();
        if (!$arResult) {
            return [];
        }

        return $arResult;
    }

    public function findBy(array $criteria): array
    {
        $arResult = EntityDispatcherTable::getList(['filter' => $criteria])->fetchAll();
        if (!$arResult) {
            return [];
        }

        return $arResult;
    }

    public function updateFields(int $id, array $arFields): void
    {
        EntityDispatcherTable::update($id, $arFields);
    }
}
