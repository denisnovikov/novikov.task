<?php

namespace Novikov\Task\Repository;

use Bitrix\Main\ORM\Objectify\EntityObject;
use Bitrix\Main\Type\DateTime;
use Novikov\Task\Exception\RepositoryFindItemException;
use Novikov\Task\Orm\EntityObjectTable;
use Novikov\Task\System\Repository;

class ObjectUnitRepository extends Repository implements RepositoryInterface
{
    private const DEFAULT_FILTER = ['DISPATCHER_ACTIVE' => 'Y',];

    public function find(int $id): ?EntityObject
    {
        $obResult = EntityObjectTable::getByPrimary([$id])->fetchObject();
        if (!$obResult) {
            throw new RepositoryFindItemException('Элемент не найден');
        }

        return $obResult;
    }

    public function findOneBy(array $criteria): ?EntityObject
    {
        $obResult = EntityObjectTable::getList(['filter' => $criteria])->fetchObject();
        if (!$obResult) {
            throw new RepositoryFindItemException('Элемент не найден');
        }

        return $obResult;
    }

    public function findAll(): array
    {
        $arResult = EntityObjectTable::getList([
            'filter' =>
                self::DEFAULT_FILTER + [
                '>=DISPATCHER_ACTIVE_TO' => new DateTime()
            ]]
        )->fetchAll();
        if (!$arResult) {
            return [];
        }

        return $arResult;
    }

    public function findBy(array $criteria): array
    {
        $arResult = EntityObjectTable::getList(['filter' => $criteria + self::DEFAULT_FILTER])->fetchAll();
        if (!$arResult) {
            return [];
        }

        return $arResult;
    }

    public function updateFields(int $id, array $arFields): void
    {
        EntityObjectTable::update($id, $arFields);
    }
}
