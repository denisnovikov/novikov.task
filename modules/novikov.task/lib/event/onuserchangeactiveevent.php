<?php

namespace Novikov\Task\Event;

use Novikov\Task\Repository\DispatcherRepository;

class OnUserChangeActiveEvent
{
    /**
     * Деактивация диспетчера при деактивации пользователя
     *
     * @param array $arFields
     * @return void
     */
    public static function OnUserUpdate(array &$arFields): void
    {
        $dispatcherRepository = DispatcherRepository::create();
        $obDispatcher = $dispatcherRepository->findOneBy([
            'USER_ID' => $arFields['ID']
        ]);

        $dispatcherRepository->updateFields($obDispatcher->getId(), [
            'ACTIVE' => $arFields['ACTIVE']]
        );
    }
}
