<?php

namespace Novikov\Task\Orm;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Entity;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Main\ORM\Query\Result;
use Bitrix\Main\SystemException;
use Bitrix\Main\Type\DateTime;

class EntityObjectTable extends Entity\DataManager
{
    public static function getTableName(): string
    {
        return 'novikov_task_entity_object';
    }

    /**
     * @throws ArgumentException
     * @throws SystemException
     */
    public static function getMap(): array
    {
        return [
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
            ]),
            new Entity\IntegerField('DISPATCHER_ID', [
                'validation' => static function () {
                    return [
                        new Entity\Validator\Unique(),
                    ];
                }
            ]),
            new Entity\ReferenceField(
                'DISPATCHER',
                EntityDispatcherTable::class,
                Join::on('this.DISPATCHER_ID', 'ref.ID')
            ),
            new Entity\DatetimeField('DATE_CREATE', [
                'default_value' => new Datetime()
            ]),
            new Entity\StringField('NAME', [
                'validation' => static function () {
                    return [
                        new Entity\Validator\Unique(),
                        new Entity\Validator\Length(1, 255)
                    ];
                }
            ]),
            new Entity\StringField('ADDRESS', [
                'validation' => static function () {
                    return [
                        new Entity\Validator\Unique(),
                        new Entity\Validator\Length(1, 255)
                    ];
                }
            ]),
            new Entity\TextField('COMMENT', [
            ]),
        ];
    }

    public static function getList(array $parameters = array()): Result
    {
        $parameters['select'] = [
            'OBJECT_ID' => 'ID',
            'OBJECT_DATE_CREATE' => 'DATE_CREATE',
            'OBJECT_NAME' => 'NAME',
            'OBJECT_ADDRESS' => 'ADDRESS',
            'OBJECT_COMMENT' => 'COMMENT',

            'DISPATCHER_DATE_CREATE' => 'DISPATCHER.DATE_CREATE',
            'DISPATCHER_ACTIVE' => 'DISPATCHER.ACTIVE',
            'DISPATCHER_ACTIVE_TO' => 'DISPATCHER.ACTIVE_TO',
            'DISPATCHER_COMMENT' => 'DISPATCHER.COMMENT',

            'USER_NAME' => 'DISPATCHER.USER.NAME',
            'USER_ROLE' => 'DISPATCHER.USER_ROLE',
            'USER_LAST_NAME' => 'DISPATCHER.USER.LAST_NAME',
            'USER_LAST_LOGIN' => 'DISPATCHER.USER.LAST_LOGIN',
        ];

        return parent::getList($parameters);
    }
}
