<?php

namespace Novikov\Task\Orm;

use Bitrix\Main\Entity;
use Bitrix\Main\SystemException;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\UserTable;
use CMain;

class EntityDispatcherTable extends Entity\DataManager
{
    public static function getTableName(): string
    {
        return 'novikov_task_entity_dispatcher';
    }

    /**
     * @return array
     * @throws SystemException
     */
    public static function getMap(): array
    {
        /** @var CMain $USER */
        global $USER;

        return [
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
            ]),
            new Entity\DatetimeField('DATE_CREATE', [
                'default_value' => new Datetime()
            ]),
            new Entity\BooleanField('ACTIVE', [
                'values' => ['Y', 'N'],
                'default_value' => 'Y'
            ]),
            new Entity\DatetimeField(
                'ACTIVE_TO', [
                ]
            ),
            new Entity\IntegerField('USER_ID', [
                'default_value' => $USER->GetID(),
                'validation' => static function () {
                    return [
                        new Entity\Validator\Unique(),
                    ];
                }
            ]),
            new Entity\ReferenceField(
                'USER',
                UserTable::class,
                ['=this.USER_ID' => 'ref.ID']
            ),
            new Entity\IntegerField(
                'USER_ROLE', [
                ]
            ),
            new Entity\TextField('COMMENT', [
                'save_data_modification' => static function () {
                    return [
                        static fn($value) => serialize($value)
                    ];
                },
                'fetch_data_modification' => static function () {
                    return [
                        static fn($value) => unserialize($value)
                    ];
                }
            ]),
        ];
    }
}
