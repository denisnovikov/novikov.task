<?php

namespace Novikov\Task\System;

abstract class Repository
{
    public static function create(): self
    {
        return new static();
    }
}
