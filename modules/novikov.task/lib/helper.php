<?php

namespace Novikov\Task;

/**
 * Class Helper
 * @package Novikov\Task
 */
class Helper
{
    /**
     * @param bool $notDocumentRoot
     * @return mixed
     */
    public static function getModulePath(bool $notDocumentRoot = false)
    {
        $oModule = \CModule::CreateModuleObject('novikov.task');

        return $oModule->GetPath($notDocumentRoot);
    }
}
