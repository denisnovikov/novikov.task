<?php

namespace Novikov\Task\Exception;

class RepositoryFindItemException extends RepositoryException
{
}
