<?php

namespace Novikov\Task\Exception;

class RepositoryAddItemException extends RepositoryException
{
}
