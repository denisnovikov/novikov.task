<?php

namespace Novikov\Task\Exception;

use Exception;

class RepositoryException extends Exception
{
}
