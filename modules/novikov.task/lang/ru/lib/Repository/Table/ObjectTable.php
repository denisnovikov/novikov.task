<?php
$MESS["OBJECT_ENTITY_ID_FIELD"] = "Внутренний идентификатор";
$MESS["OBJECT_ENTITY_DISPATCHER_ID_FIELD"] = "Идентификатор диспетчера";
$MESS["OBJECT_ENTITY_DATE_CREATE_FIELD"] = "Дата и время создание записи";
$MESS["OBJECT_ENTITY_NAME_FIELD"] = "Наименование";
$MESS["OBJECT_ENTITY_ADDRESS_FIELD"] = "Адрес";
$MESS["OBJECT_ENTITY_COMMENT_FIELD"] = "Комментарий";
