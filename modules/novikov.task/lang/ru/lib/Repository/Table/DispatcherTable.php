<?php
$MESS["DISPATCHER_ENTITY_ID_FIELD"] = "Внутренний идентификатор";
$MESS["DISPATCHER_ENTITY_DATE_CREATE_FIELD"] = "Дата и время создание записи";
$MESS["DISPATCHER_ENTITY_ACTIVE_FIELD"] = "Активность";
$MESS["DISPATCHER_ENTITY_ACTIVE_TO_FIELD"] = "Дата окончания активности";
$MESS["DISPATCHER_ENTITY_USER_ID_FIELD"] = "Привязка к таблице пользователей (b_user)";
$MESS["DISPATCHER_ENTITY_USER_ROLE_FIELD"] = "Уровень доступа (число от 1 до 12)";
$MESS["DISPATCHER_ENTITY_COMMENT_FIELD"] = "Комментарий";
