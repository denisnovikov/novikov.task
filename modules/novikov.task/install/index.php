<?

IncludeModuleLangFile(__FILE__);

use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\DB\SqlQueryException;
use Bitrix\Main\Entity\Base;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\SystemException;
use Novikov\Task\Event\OnUserChangeActiveEvent;
use Novikov\Task\Helper;
use Novikov\Task\Orm\EntityDispatcherTable;
use Novikov\Task\Orm\EntityObjectTable;

class Novikov_Task extends CModule
{
    public $MODULE_ID;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;

    public array $arOrmTables = [
        EntityDispatcherTable::class,
        EntityObjectTable::class,
    ];

    public function __construct()
    {
        $arModuleVersion = [];
        include(__DIR__ . "/version.php");

        $this->MODULE_ID = 'novikov.task';
        $this->MODULE_NAME = Loc::GetMessage("NOVIKOV_DISPATCHER_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::GetMessage("NOVIKOV_DISPATCHER_MODULE_DESC");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
    }

    /**
     * Установка модуля
     *
     * @return void
     * @throws ArgumentException
     * @throws LoaderException
     * @throws SystemException
     */
    public function DoInstall(): void
    {
        global $APPLICATION;
        RegisterModule($this->MODULE_ID);

        if ($this->isVersionD7()) {
            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();
        } else {
            $APPLICATION->ThrowException(Loc::getMessage('NOVIKOV_DISPATCHER_INSTALL_ERROR_VERSION'));
        }
    }

    private function isVersionD7(): bool
    {
        return CheckVersion(ModuleManager::getVersion('main'), '14.00.00');
    }

    /**
     * @throws LoaderException
     * @throws ArgumentException
     * @throws SystemException
     */
    public function installDB(): void
    {
        Loader::includeModule($this->MODULE_ID);
        foreach ($this->arOrmTables as $table) {
            if (!Application::getConnection()->isTableExists(Base::getInstance($table)->getDBTableName())) {
                Base::getInstance($table)->createDbTable();
            }
        }
    }

    public function InstallEvents(): void
    {
        RegisterModuleDependences(
            'main', 'OnAfterUserUpdate', 'novikov.task',
            OnUserChangeActiveEvent::class, 'OnUserUpdate');
    }

    public function InstallFiles($arParams = []): void
    {
        CopyDirFiles(
            Application::getInstance()->getContext()->getServer()->getDocumentRoot() . Helper::getModulePath(true) . "/install/components",
            Application::getInstance()->getContext()->getServer()->getDocumentRoot() . "/local/components",
            true,
            true
        );
    }

    /**
     * @throws ArgumentException
     * @throws SqlQueryException
     * @throws LoaderException
     * @throws SystemException
     */
    public function DoUninstall(): void
    {
        $this->uninstallFiles();
        $this->uninstallEvents();
        $this->uninstallDB();

        UnRegisterModule($this->MODULE_ID);
    }

    public function UnInstallFiles(): void
    {
        DeleteDirFilesEx(Application::getInstance()->getContext()->getServer()->getDocumentRoot() . "/local/components/" . $this->MODULE_ID);
    }

    public function UnInstallEvents(): void
    {
        UnRegisterModuleDependences(
            'main', 'OnAfterUserUpdate', 'novikov.task',
            OnUserChangeActiveEvent::class, 'OnUserUpdate');
    }

    /**
     * @throws LoaderException
     * @throws ArgumentException
     * @throws SqlQueryException
     * @throws SystemException
     */
    public function uninstallDB(): void
    {
        Loader::includeModule($this->MODULE_ID);
        foreach ($this->arOrmTables as $table) {
            Application::getConnection()->queryExecute(
                'DROP TABLE IF EXISTS ' . Base::getInstance($table)->getDBTableName()
            );
        }
    }

    /**
     * @param bool $notDocumentRoot
     * @return array|string|string[]
     */
    public function GetPath(bool $notDocumentRoot = false)
    {
        if ($notDocumentRoot) {
            return str_ireplace(Application::getDocumentRoot(), '', str_replace('\\', '/', dirname(__DIR__)));
        } else {
            return dirname(__DIR__);
        }
    }
}
