<?

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Novikov\Task\Entity\ObjectUnit;
use Novikov\Task\Repository\DispatcherRepository;
use Novikov\Task\Repository\ObjectUnitRepository;

/** @var CUser $USER */
global $USER;

Loc::loadMessages(__FILE__);

class novikov_object_list_component extends CBitrixComponent
{
    public array $errors = [];

    public DispatcherRepository $dispatcherRepository;
    public ObjectUnitRepository $objectRepository;

    public function onPrepareComponentParams(array $arParams): array
    {
        if (!Loader::includeModule('novikov.task')) {
            $this->errors[] = Loc::getMessage('NOVIKOV_DISPATCHER_MODULE_EXISTS');
        }

        if (!class_exists(DispatcherRepository::class) && !class_exists(ObjectUnitRepository::class)) {
            $this->errors[] = Loc::getMessage('NOVIKOV_DISPATCHER_CLASS_EXISTS');
        }

        return parent::onPrepareComponentParams($arParams);
    }

    public function executeComponent(): void
    {
        global $CACHE_MANAGER;

        if (!empty($this->errors)) {
            foreach ($this->errors as $error) {
                ShowError($error);
            }
            return;
        }

        if ($this->StartResultCache()) {
            $relativePath = $CACHE_MANAGER->getCompCachePath($this->getRelativePath());
            $CACHE_MANAGER->StartTagCache($relativePath);
            $CACHE_MANAGER->RegisterTag("Novikov.Dispatcher");
            $this->arResult['OBJECT_TABLE_HEADER'] = $this->getObjectTableHeader();
            $this->arResult['OBJECT_UNITS'] = $this->getObjectUnits();
            $CACHE_MANAGER->EndTagCache();
        }

        $this->includeComponentTemplate();
    }

    protected function listKeysSignedParameters(): array
    {
        return [
            'CACHE_TYPE',
            'CACHE_TIME',
        ];
    }

    private function getObjectTableHeader(): array
    {
        return [
            'Фамилия',
            'Имя',
            'Уровень доступа',
            'Дата и время последнего входа в систему',
            'Комментарий',
            'Объект'
        ];
    }

    private function getObjectUnits(): array
    {
        return array_map(static function (array $entity) {
            return ObjectUnit::fromArray($entity);
        }, ObjectUnitRepository::create()->findAll() ?? []);

    }
}
