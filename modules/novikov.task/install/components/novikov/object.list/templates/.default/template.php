<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Novikov\Task\Entity\ObjectUnit;

/** @var array $arParams */
/** @var array $arResult */
/** @var ObjectUnit $objectUnit */
?>

<div class="container">
    <div class="row">
        <table class="table table-bordered">
            <tr><?
                foreach ($arResult['OBJECT_TABLE_HEADER'] as $objectHeader) { ?>
                    <th><?= $objectHeader ?></th><?
                } ?>
            </tr><?
            foreach ($arResult['OBJECT_UNITS'] as $objectUnit) { ?>
                <tr>
                    <td><?= $objectUnit->userLastName ?></td>
                    <td><?= $objectUnit->userName ?></td>
                    <td><?= $objectUnit->userRole ?></td>
                    <td><?= $objectUnit->userLastLogin ?></td>
                    <td><?= $objectUnit->dispatcherComment ?></td>
                    <td><?= $objectUnit->objectName ?></td>
                </tr><?
            } ?>
        </table>
    </div>
</div>
