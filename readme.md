# Тестовое задание для разработчика на платформе Битрикс

После установки модуля, необходимо вызвать компонент на любой странице:

```php
<? $APPLICATION->IncludeComponent("novikov:object.list", ".default", [], false); ?>
```

Для простого добавления записей, достаточно вызвать код на любой странице:

```php
\Bitrix\Main\Loader::includeModule('novikov.task');

\Novikov\Task\Orm\EntityDispatcherTable::add([
    'ACTIVE_TO' => new \Bitrix\Main\Type\DateTime('01.01.2024'),
    'COMMENT' => 'COMMENT 1'
]);

\Novikov\Task\Orm\EntityObjectTable::add([
    'DISPATCHER_ID' => 1,
    'NAME' => 'NAME 1',
    'ADDRESS' => 'ADDRESS 1',
    'COMMENT' => 'COMMENT 1',
]);
```
